using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour, IGameUIController
{
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TextMeshProUGUI _livesText;
    [SerializeField] private GameObject _gameOverPanel;

    private void Start()
    {
        _gameOverPanel.SetActive(false);
        Contexts.sharedInstance.game.SetUI(this);
    }

    public void SetScore(int score)
    {
        _scoreText.text = $"score: {score}";
    }

    public void SetLives(int current, int max)
    {
        _livesText.text = $"lives: {current}/{max}";
    }

    public void SetGameOver()
    {
        _gameOverPanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}