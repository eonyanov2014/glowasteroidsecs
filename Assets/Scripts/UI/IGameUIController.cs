﻿public interface IGameUIController
{
    void SetScore(int score);
    void SetLives(int current, int max);
    void SetGameOver();
}