using System;
using Entitas;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameConfig _gameConfig;
    
    private UpdateSystems _updateSystems;
    private FixedUpdateSystems _fixedUpdateSystems;

    // Start is called before the first frame update
    void Start()
    {
        Contexts.sharedInstance.game.SetGameConfig(_gameConfig);
        Contexts.sharedInstance.game.SetPlayerInput(0,0,false);
        Contexts.sharedInstance.game.SetScore(0);

        _updateSystems = new UpdateSystems(Contexts.sharedInstance);
        _updateSystems.Initialize();
        
        _fixedUpdateSystems = new FixedUpdateSystems(Contexts.sharedInstance);
        _fixedUpdateSystems.Initialize();

        Contexts.sharedInstance.game.SetAsteroidsCounter(0);
    }

    private void Update()
    {
        _updateSystems.Execute();
    }

    private void FixedUpdate()
    {
        _fixedUpdateSystems.Execute();
    }

    private void OnDestroy()
    {
        _updateSystems.TearDown();
        _fixedUpdateSystems.TearDown();
        _updateSystems.DeactivateReactiveSystems();
        _fixedUpdateSystems.DeactivateReactiveSystems();
        
        Contexts.sharedInstance.game.Reset();
        Contexts.sharedInstance.input.Reset();
    }
}
