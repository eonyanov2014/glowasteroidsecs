﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerOnTriggerEnterHandler : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;

    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var entity = Contexts.sharedInstance.game.CreateEntity();
        entity.AddPlayerCollision(_rigidbody2D);
    }
}