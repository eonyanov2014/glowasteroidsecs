﻿public enum AsteroidType
{
    Small = 1,
    Mid = 2,
    Big = 3
}