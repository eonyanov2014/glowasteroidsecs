﻿using System;
using UnityEngine;

[Serializable]
public class AsteroidsConfig
{
    public GameObject[] Prefabs;
    public float Speed;
    public int Health;
    public float Radius;
    public int Score;
    public GameObject ExplosionParticles;

    public GameObject GetRandomPrefab()
    {
        return Prefabs[UnityEngine.Random.Range(0, Prefabs.Length)];
    }
}