﻿using System;
using UnityEngine;

[Serializable]
public class PlayerConfig
{
    public GameObject Prefab;
    public float Acceleration;
    public float MaxSpeed;
    public float RotationSpeed;
    public int Lives;
    public GameObject ExplosionPrefab;
}