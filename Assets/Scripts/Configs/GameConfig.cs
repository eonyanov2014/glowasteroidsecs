﻿using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[CreateAssetMenu]
[Game, Unique]
public class GameConfig : ScriptableObject
{
    public PlayerConfig PlayerConfig;
    public GameObject ShotPrefab;
    public float ShotSpeed;

    public AsteroidsConfig BigAsteroids;
    public AsteroidsConfig MidAsteroids;
    public AsteroidsConfig SmallAsteroids;
}