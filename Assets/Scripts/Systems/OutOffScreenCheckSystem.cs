﻿using Entitas;
using UnityEngine;

public class OutOffScreenCheckSystem : IExecuteSystem
{
    private Contexts _contexts;

    private IGroup<GameEntity> _group;

    public OutOffScreenCheckSystem(Contexts contexts)
    {
        _contexts = contexts;
        _group = _contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.OutOffScreenCheck, GameMatcher.Rigidbody));
    }

    public void Execute()
    {
        var borders = _contexts.game.screenBorders;

        foreach (var e in _group)
        {
            var pos = e.rigidbody.Rigidbody.position;
            var newPos = pos;
            var radius = e.outOffScreenCheck.Radius;
            
            if (pos.x < -borders.HalfWidth - radius)
            {
                newPos.x += (borders.HalfWidth + radius) * 2f;
                e.AddWrap(newPos);
                return;
            }

            if (pos.x > borders.HalfWidth + radius)
            {
                newPos.x -= (borders.HalfWidth + radius) * 2f;
                e.AddWrap(newPos);
                return;
            }

            if (pos.y < -borders.HalfHeight - radius)
            {
                newPos.y += (borders.HalfHeight + radius) * 2f;
                e.AddWrap(newPos);
                return;
            }

            if (pos.y > borders.HalfHeight + radius)
            {
                newPos.y -= (borders.HalfHeight + radius) * 2f;
                e.AddWrap(newPos);
                return;
            }
        }
    }
}