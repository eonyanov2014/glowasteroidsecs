﻿using Entitas;
using System.Collections.Generic;
using System.Linq;

public class AsteroidsHitSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public AsteroidsHitSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.ShotCollision));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasShotCollision;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            var shot = e.shotCollision.Shot;
            var shotEntity = _contexts.game.GetEntitiesWithRigidbody(shot).First();
            shotEntity.isDestroy = true;

            var asteroid = e.shotCollision.Asteroid;
            var asteroidEntity = _contexts.game.GetEntitiesWithRigidbody(asteroid).First();
            asteroidEntity.ReplaceHealth(asteroidEntity.health.Value - 1);
            e.isDestroy = true;
        }
    }
}