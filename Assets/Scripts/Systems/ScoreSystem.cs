﻿using Entitas;
using System.Collections.Generic;

public class ScoreSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public ScoreSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Score);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasScore;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        _contexts.game.uI.GameUIController.SetScore(_contexts.game.score.Value);
    }
}