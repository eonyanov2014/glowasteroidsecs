﻿using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class CreateParticlesSystems : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public CreateParticlesSystems(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Particles);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasParticles;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            Object.Instantiate(e.particles.Prefab, e.particles.Position, Quaternion.identity);
            e.isDestroy = true;
        }
    }
}