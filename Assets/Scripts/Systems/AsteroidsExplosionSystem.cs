﻿using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsExplosionSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public AsteroidsExplosionSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Asteroid, GameMatcher.Health));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasAsteroid && entity.hasHealth;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            if (e.health.Value > 0) continue;

            if (e.asteroid.Type != AsteroidType.Small)
            {
                for (var i = 0; i < 3; i++)
                {
                    var newAsteroid = _contexts.game.CreateEntity();
                    newAsteroid.AddAsteroid(e.asteroid.Type - 1);
                    newAsteroid.AddStartPosition(e.rigidbody.Rigidbody.position + Random.insideUnitCircle.normalized * 0.5f);
                }
            }

            CreateParticles(e);
            e.isDestroy = true;
            _contexts.game.ReplaceAsteroidsCounter(_contexts.game.asteroidsCounter.Value - 1);
        }
    }

    private void CreateParticles(GameEntity e)
    {
        var asteroidConfig = GetConfig(e.asteroid.Type);
        var particles = _contexts.game.CreateEntity();
        particles.AddParticles(asteroidConfig.ExplosionParticles, e.rigidbody.Rigidbody.position);
        _contexts.game.ReplaceScore(_contexts.game.score.Value + asteroidConfig.Score);
    }
    
    AsteroidsConfig GetConfig(AsteroidType asteroidType)
    {
        return asteroidType switch
        {
            AsteroidType.Small => _contexts.game.gameConfig.value.SmallAsteroids,
            AsteroidType.Mid => _contexts.game.gameConfig.value.MidAsteroids,
            AsteroidType.Big => _contexts.game.gameConfig.value.BigAsteroids,
            _ => null
        };
    }
}