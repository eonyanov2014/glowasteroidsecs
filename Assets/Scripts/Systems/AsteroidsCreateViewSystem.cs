﻿using Entitas;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class AsteroidsCreateViewSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public AsteroidsCreateViewSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Asteroid.Added());
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasAsteroid;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            CreateAsteroidView(e);
        }
    }

    private void CreateAsteroidView(GameEntity e)
    {
        var config = GetConfig(e.asteroid.Type);
        var go = Object.Instantiate(config.GetRandomPrefab(), e.startPosition.Position, quaternion.identity);
        var rb = go.GetComponent<Rigidbody2D>();
        e.AddRigidbody(rb);
        e.AddHealth(config.Health);
        e.AddOutOffScreenCheck(config.Radius);
        rb.velocity = Random.insideUnitCircle.normalized * config.Speed;
        _contexts.game.ReplaceAsteroidsCounter(_contexts.game.asteroidsCounter.Value + 1);
    }

    AsteroidsConfig GetConfig(AsteroidType asteroidType)
    {
        return asteroidType switch
        {
            AsteroidType.Small => _contexts.game.gameConfig.value.SmallAsteroids,
            AsteroidType.Mid => _contexts.game.gameConfig.value.MidAsteroids,
            AsteroidType.Big => _contexts.game.gameConfig.value.BigAsteroids,
            _ => null
        };
    }
}