﻿using Entitas;
using UnityEngine;

public class PlayerInputSystem : IExecuteSystem
{
    private readonly Contexts _contexts;

    public PlayerInputSystem(Contexts contexts)
    {
        _contexts = contexts;
    }

    public void Execute()
    {
        _contexts.game.ReplacePlayerInput(Input.GetAxis("Vertical") > 0f ? 1f : 0,
            Input.GetAxis("Horizontal"),
            Input.GetButtonDown("Jump"));
    }
}