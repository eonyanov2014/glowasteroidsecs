﻿using Entitas;
using UnityEngine;

public class PlayerMoveSystem : IExecuteSystem
{
    private Contexts _contexts;

    private IGroup<GameEntity> _entities;

    public PlayerMoveSystem(Contexts contexts)
    {
        _contexts = contexts;
        _entities = _contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Rigidbody, GameMatcher.Player));
    }

    public void Execute()
    {
        foreach (var e in _entities)
        {
            var playerConfig = _contexts.game.gameConfig.value.PlayerConfig;
            var rb = e.rigidbody.Rigidbody;
            rb.AddRelativeForce(_contexts.game.playerInput.Acceleration * Vector2.up * playerConfig.Acceleration);
            rb.velocity = Vector2.ClampMagnitude(rb.velocity, playerConfig.MaxSpeed);
            rb.MoveRotation(rb.rotation - _contexts.game.playerInput.Turn * playerConfig.RotationSpeed);
        }
    }
}