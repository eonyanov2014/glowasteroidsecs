﻿using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class DestroySystem : ReactiveSystem<GameEntity> {
    private Contexts _contexts;

	public DestroySystem (Contexts contexts) : base(contexts.game) {
        _contexts = contexts;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.Destroy);
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.isDestroy;
	}

	protected override void Execute(List<GameEntity> entities) {
		foreach (var e in entities) {
			if (e.hasRigidbody)
			{
				Object.Destroy(e.rigidbody.Rigidbody.gameObject);
			}
			e.Destroy();
		}
	}
}