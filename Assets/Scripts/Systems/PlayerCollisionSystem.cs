﻿using Entitas;
using System.Collections.Generic;
using System.Linq;

public class PlayerCollisionSystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts _contexts;
    public PlayerCollisionSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.PlayerCollision);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasPlayerCollision;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            var player = e.playerCollision.Player;
            var entity = _contexts.game.GetEntitiesWithRigidbody(player).First();
            entity.isDestroy = true;
           
            var particles = _contexts.game.CreateEntity();
            particles.AddParticles(_contexts.game.gameConfig.value.PlayerConfig.ExplosionPrefab, player.position);
            
            _contexts.game.ReplacePlayerLives(_contexts.game.playerLives.Value - 1);
            
            e.Destroy();
        }
    }
}