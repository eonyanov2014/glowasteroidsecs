﻿using Entitas;
using UnityEngine;

public class ShootSystem : IExecuteSystem
{
    private Contexts _contexts;

    private IGroup<GameEntity> _entities;

    public ShootSystem(Contexts contexts)
    {
        _contexts = contexts;
        _entities = _contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Player));
    }

    public void Execute()
    {
        if(!_contexts.game.playerInput.Shoot) return;
        foreach (var e in _entities)
        {
            CreateProjectile(e.player.View.transform.position, e.player.View.transform.up);
        }
    }

    private void CreateProjectile(Vector3 position, Vector2 direction)
    {
        var go = Object.Instantiate(_contexts.game.gameConfig.value.ShotPrefab);
        go.transform.position = position;
        var entity = _contexts.game.CreateEntity();
        entity.isShot = true;
        entity.AddOutOffScreenCheck(0.5f);
        var rb = go.GetComponent<Rigidbody2D>();
        if(rb == null) return;
        entity.AddRigidbody(rb);
        rb.velocity = direction * _contexts.game.gameConfig.value.ShotSpeed;
    }
}