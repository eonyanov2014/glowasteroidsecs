﻿using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsWaveSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public AsteroidsWaveSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }
    

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AsteroidsCounter);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasAsteroidsCounter;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        if(_contexts.game.asteroidsCounter.Value == 0)
            GenerateAsteroids();
    }

    private void GenerateAsteroids()
    {
        var position = Vector3.zero;
        var player = _contexts.game.GetEntities(GameMatcher.Player);
        if (player.Length > 0)
            position = player[0].player.View.transform.position;
        
        for (var i = 0; i < 2; i++)
        {
            var e = _contexts.game.CreateEntity();
            e.AddAsteroid(AsteroidType.Big);
            e.AddStartPosition(position + (Vector3) Random.insideUnitCircle.normalized * Random.Range(3f, 5f));
        }
    }
}