﻿using Entitas;
using UnityEngine;

public class InitializeScreenBorderSystem : IInitializeSystem
{
    private Contexts _contexts;

    public InitializeScreenBorderSystem(Contexts contexts)
    {
        _contexts = contexts;
    }

    public void Initialize()
    {
        var camera = Camera.main;
        var screenWidth = camera.ViewportToWorldPoint(new Vector3(1, 0, -10)).x;
        var screenHeight = camera.ViewportToWorldPoint(new Vector3(0, 1, -10)).y;
        _contexts.game.SetScreenBorders(screenWidth, screenHeight);
    }
}