﻿using Entitas;
using System.Collections.Generic;

public class GameOverSystem : ReactiveSystem<GameEntity> {
    private Contexts _contexts;

	public GameOverSystem (Contexts contexts) : base(contexts.game) {
        _contexts = contexts;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.GameOver);
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.isGameOver;
	}

	protected override void Execute(List<GameEntity> entities)
	{
		_contexts.game.uI.GameUIController.SetGameOver();
	}
}