﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class CreatePlayerSystem :  ReactiveSystem<GameEntity>, IInitializeSystem
{
    private readonly Contexts _contexts;

    public CreatePlayerSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    public void Initialize()
    {
        _contexts.game.SetPlayerLives(_contexts.game.gameConfig.value.PlayerConfig.Lives);
    }

    private void CreatePlayer()
    {
        _contexts.game.uI.GameUIController.SetLives(_contexts.game.playerLives.Value, _contexts.game.gameConfig.value.PlayerConfig.Lives);
        if (_contexts.game.playerLives.Value == 0)
        {
            var gameOver =_contexts.game.CreateEntity();
            gameOver.isGameOver = true;
            return;
        }
        
        var playerEntity = _contexts.game.CreateEntity();
        var go = Object.Instantiate(_contexts.game.gameConfig.value.PlayerConfig.Prefab);
        playerEntity.AddPlayer(go);
        playerEntity.AddOutOffScreenCheck(0.5f);
        var rb = go.GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            Debug.LogError("Player does not have Rigidbody!");
            return;
        }

        playerEntity.AddRigidbody(rb);
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.PlayerLives);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasPlayerLives;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        Debug.Log("Create Player");
        CreatePlayer();
    }
}