﻿public sealed class FixedUpdateSystems : Feature  {
    public FixedUpdateSystems(Contexts contexts)
    {
        Add(new PlayerMoveSystem(contexts));
        Add(new OutOffScreenCheckSystem(contexts));
        Add(new WrapSystem(contexts));
        Add(new DestroyShotSystem(contexts));
    }	
}