﻿using Entitas;
using System.Collections.Generic;

public class DestroyShotSystem : ReactiveSystem<GameEntity> {
    private Contexts _contexts;

	public DestroyShotSystem (Contexts contexts) : base(contexts.game) {
        _contexts = contexts;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.Wrap);
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.isShot;
	}

	protected override void Execute(List<GameEntity> entities) {
		foreach (var e in entities)
		{
			e.isDestroy = true;
		}
	}
}