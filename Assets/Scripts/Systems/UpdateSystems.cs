﻿public sealed class UpdateSystems : Feature
{
    public UpdateSystems(Contexts contexts)
    {
        Add(new InitializeScreenBorderSystem(contexts));
        Add(new PlayerInputSystem(contexts));
        Add(new CreatePlayerSystem(contexts));
        Add(new ShootSystem(contexts));
        Add(new AsteroidsCreateViewSystem(contexts));
        Add(new AsteroidsWaveSystem(contexts));
        Add(new AsteroidsHitSystem(contexts));
        Add(new AsteroidsExplosionSystem(contexts));
        Add(new PlayerCollisionSystem(contexts));
        Add(new CreateParticlesSystems(contexts));
        Add(new GameOverSystem(contexts));
        Add(new ScoreSystem(contexts));
        Add(new DestroySystem(contexts));
    }
}