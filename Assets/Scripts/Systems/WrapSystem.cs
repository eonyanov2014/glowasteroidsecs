﻿using Entitas;
using System.Collections.Generic;

public class WrapSystem : ReactiveSystem<GameEntity> {
    private Contexts _contexts;

	public WrapSystem (Contexts contexts) : base(contexts.game) {
        _contexts = contexts;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.Wrap);
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.hasAsteroid || entity.hasPlayer;
	}

	protected override void Execute(List<GameEntity> entities) {
		foreach (var e in entities) 
		{
			e.rigidbody.Rigidbody.MovePosition(e.wrap.NewPosition);
			e.RemoveWrap();
		}
	}
}