﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class PlayerInputComponent : IComponent
{
    public float Acceleration;
    public float Turn;
    public bool Shoot;
}