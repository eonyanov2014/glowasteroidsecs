﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class PlayerLivesComponent : IComponent
{
    public int Value;
}