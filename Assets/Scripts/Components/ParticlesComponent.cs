﻿using Entitas;
using UnityEngine;

[Game]
public class ParticlesComponent : IComponent
{
    public GameObject Prefab;
    public Vector3 Position;
}