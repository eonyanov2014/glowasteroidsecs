﻿using Entitas;
using UnityEngine;

[Game]
public class StartPositionComponent : IComponent
{
    public Vector2 Position;
}