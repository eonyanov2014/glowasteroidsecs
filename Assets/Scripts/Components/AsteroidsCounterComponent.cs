﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class AsteroidsCounterComponent : IComponent
{
    public int Value;
}