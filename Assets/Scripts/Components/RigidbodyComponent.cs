﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game]
public class RigidbodyComponent : IComponent
{
    [EntityIndex]
    public Rigidbody2D Rigidbody;
}