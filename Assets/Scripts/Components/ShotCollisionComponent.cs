﻿using Entitas;
using UnityEngine;

[Game]
public class ShotCollisionComponent : IComponent
{
    public Rigidbody2D Shot;
    public Rigidbody2D Asteroid;
}