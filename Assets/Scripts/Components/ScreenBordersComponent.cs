﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class ScreenBordersComponent : IComponent
{
    public float HalfWidth;
    public float HalfHeight;
}