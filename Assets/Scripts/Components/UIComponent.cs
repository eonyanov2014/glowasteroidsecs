﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class UIComponent : IComponent
{
    public IGameUIController GameUIController;
}