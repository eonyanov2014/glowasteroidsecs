﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class ScoreComponent : IComponent
{
    public int Value;
}