﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ShotOnTriggerEnterHandler : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;

    private void OnValidate()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var entity = Contexts.sharedInstance.game.CreateEntity();
        entity.AddShotCollision(_rigidbody2D, other.attachedRigidbody);
    }
}