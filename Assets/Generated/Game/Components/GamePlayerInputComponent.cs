//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity playerInputEntity { get { return GetGroup(GameMatcher.PlayerInput).GetSingleEntity(); } }
    public PlayerInputComponent playerInput { get { return playerInputEntity.playerInput; } }
    public bool hasPlayerInput { get { return playerInputEntity != null; } }

    public GameEntity SetPlayerInput(float newAcceleration, float newTurn, bool newShoot) {
        if (hasPlayerInput) {
            throw new Entitas.EntitasException("Could not set PlayerInput!\n" + this + " already has an entity with PlayerInputComponent!",
                "You should check if the context already has a playerInputEntity before setting it or use context.ReplacePlayerInput().");
        }
        var entity = CreateEntity();
        entity.AddPlayerInput(newAcceleration, newTurn, newShoot);
        return entity;
    }

    public void ReplacePlayerInput(float newAcceleration, float newTurn, bool newShoot) {
        var entity = playerInputEntity;
        if (entity == null) {
            entity = SetPlayerInput(newAcceleration, newTurn, newShoot);
        } else {
            entity.ReplacePlayerInput(newAcceleration, newTurn, newShoot);
        }
    }

    public void RemovePlayerInput() {
        playerInputEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PlayerInputComponent playerInput { get { return (PlayerInputComponent)GetComponent(GameComponentsLookup.PlayerInput); } }
    public bool hasPlayerInput { get { return HasComponent(GameComponentsLookup.PlayerInput); } }

    public void AddPlayerInput(float newAcceleration, float newTurn, bool newShoot) {
        var index = GameComponentsLookup.PlayerInput;
        var component = (PlayerInputComponent)CreateComponent(index, typeof(PlayerInputComponent));
        component.Acceleration = newAcceleration;
        component.Turn = newTurn;
        component.Shoot = newShoot;
        AddComponent(index, component);
    }

    public void ReplacePlayerInput(float newAcceleration, float newTurn, bool newShoot) {
        var index = GameComponentsLookup.PlayerInput;
        var component = (PlayerInputComponent)CreateComponent(index, typeof(PlayerInputComponent));
        component.Acceleration = newAcceleration;
        component.Turn = newTurn;
        component.Shoot = newShoot;
        ReplaceComponent(index, component);
    }

    public void RemovePlayerInput() {
        RemoveComponent(GameComponentsLookup.PlayerInput);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPlayerInput;

    public static Entitas.IMatcher<GameEntity> PlayerInput {
        get {
            if (_matcherPlayerInput == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PlayerInput);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPlayerInput = matcher;
            }

            return _matcherPlayerInput;
        }
    }
}
